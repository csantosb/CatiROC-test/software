#include "ftd2xx.h"
#include "WinTypes.h"
#include "string.h"
#include "top.h"
#include "unistd.h"

/// * Cards Parameters

char ControlRegister[1];

/*
  dac_offset_tr_ext_plus = 26241;
  dac_offset_tr_ext_minus = 39295;
  dac_offset_test_plus = 23232;
  dac_offset_test_minus = 42304;
  dac_offset_offset_plus = 26214;
  dac_offset_offset_minus = 39322;
  dac_offset_tr_plus = 27404;
  dac_offset_tr_minus = 38132;
*/

// default to 1 Volt., not open, null adc offset, null digital threshold, null trigger type,
// frequency master / 500, all offsets to mid-range
Card AllCards[NumMaxOfDevices] = {
    {.Escale = 0,.IsOpen = 0,.ADCOffset = 0,.DigitalThreshold = 0,.TriggerType = 0, .VGA1=0, .VGA2=0,
     .Frequency = 500, .DACOffset_tr_ext_plus = 26241, .DACOffset_tr_ext_minus = 39295,
     .DACOffset_test_plus = 23232, .DACOffset_test_minus = 42304, .DACOffset_offset_plus = 26214,
     .DACOffset_offset_minus = 39322, .DACOffset_tr_plus = 27404, .DACOffset_tr_minus = 38132},
    {.Escale = 0,.IsOpen = 0,.ADCOffset = 0,.DigitalThreshold = 0,.TriggerType = 0, .VGA1=0, .VGA2=0,
     .Frequency = 500, .DACOffset_tr_ext_plus = 26241, .DACOffset_tr_ext_minus = 39295,
     .DACOffset_test_plus = 23232, .DACOffset_test_minus = 42304, .DACOffset_offset_plus = 26214,
     .DACOffset_offset_minus = 39322, .DACOffset_tr_plus = 27404, .DACOffset_tr_minus = 38132},
    {.Escale = 0,.IsOpen = 0,.ADCOffset = 0,.DigitalThreshold = 0,.TriggerType = 0, .VGA1=0, .VGA2=0,
     .Frequency = 500, .DACOffset_tr_ext_plus = 26241, .DACOffset_tr_ext_minus = 39295,
     .DACOffset_test_plus = 23232, .DACOffset_test_minus = 42304, .DACOffset_offset_plus = 26214,
     .DACOffset_offset_minus = 39322, .DACOffset_tr_plus = 27404, .DACOffset_tr_minus = 38132},
    {.Escale = 0,.IsOpen = 0,.ADCOffset = 0,.DigitalThreshold = 0,.TriggerType = 0, .VGA1=0, .VGA2=0,
     .Frequency = 500, .DACOffset_tr_ext_plus = 26241, .DACOffset_tr_ext_minus = 39295,
     .DACOffset_test_plus = 23232, .DACOffset_test_minus = 42304, .DACOffset_offset_plus = 26214,
     .DACOffset_offset_minus = 39322, .DACOffset_tr_plus = 27404, .DACOffset_tr_minus = 38132},
    {.Escale = 0,.IsOpen = 0,.ADCOffset = 0,.DigitalThreshold = 0,.TriggerType = 0, .VGA1=0, .VGA2=0,
     .Frequency = 500, .DACOffset_tr_ext_plus = 26241, .DACOffset_tr_ext_minus = 39295,
     .DACOffset_test_plus = 23232, .DACOffset_test_minus = 42304, .DACOffset_offset_plus = 26214,
     .DACOffset_offset_minus = 39322, .DACOffset_tr_plus = 27404, .DACOffset_tr_minus = 38132},
};

/// * GETTERS

ULONG GetVGA(int CardNb, int VGA_nb, DWORD *myVGA){

    if (VGA_nb ==  1)

	*myVGA = AllCards[CardNb].VGA1;

    else

	*myVGA = AllCards[CardNb].VGA2;

    return 0;
}

ULONG GetTriggerType(int CardNb, DWORD *myTriggerType){

    *myTriggerType = AllCards[CardNb].TriggerType;

    return 0;
}

ULONG GetDigitalThreshold(int CardNb, DWORD *myDigitalThreshold){

    *myDigitalThreshold = AllCards[CardNb].DigitalThreshold;

    return 0;
}

ULONG GetFrequency(int CardNb, unsigned short int *myFrequency){

    *myFrequency = AllCards[CardNb].Frequency;

    return 0;
}

ULONG GetControlRegister(int CardNb, char ControlRegisterOut[1]){

    ftStatus = UpdateControlRegister(CardNb);
    checkout

	strcpy(ControlRegisterOut, ControlRegister);

    return 0;

}

ULONG GetIsOpen(int CardNb, DWORD *myIsOpen){

    *myIsOpen = AllCards[CardNb].IsOpen;

    return 0;
}


ULONG GetEscale(int CardNb, DWORD *myEscale){

    //*myEscale = Escale[CardNb];
    *myEscale = AllCards[CardNb].Escale;

    return 0;
}

ULONG GetADCOffset(int CardNb, DWORD *myADCOffset){

    *myADCOffset = AllCards[CardNb].ADCOffset;

    return 0;
}

/// ** DAC

ULONG GetDACOffset_tr_ext_plus(int CardNb, unsigned long int *myDACOffset_tr_ext_plus){
    *myDACOffset_tr_ext_plus = AllCards[CardNb].DACOffset_tr_ext_plus;
    return 0;
}

ULONG GetDACOffset_tr_ext_minus(int CardNb, unsigned long int *myDACOffset_tr_ext_minus){
    *myDACOffset_tr_ext_minus = AllCards[CardNb].DACOffset_tr_ext_minus;
    return 0;
}

ULONG GetDACOffset_test_plus(int CardNb, unsigned long int *myDACOffset_test_plus){
    *myDACOffset_test_plus = AllCards[CardNb].DACOffset_test_plus;
    return 0;
}

ULONG GetDACOffset_test_minus(int CardNb, unsigned long int *myDACOffset_test_minus){
    *myDACOffset_test_minus = AllCards[CardNb].DACOffset_test_minus;
    return 0;
}

ULONG GetDACOffset_offset_plus(int CardNb, unsigned long int *myDACOffset_offset_plus){
    *myDACOffset_offset_plus = AllCards[CardNb].DACOffset_offset_plus;
    return 0;
}

ULONG GetDACOffset_offset_minus(int CardNb, unsigned long int *myDACOffset_offset_minus){
    *myDACOffset_offset_minus = AllCards[CardNb].DACOffset_offset_minus;
    return 0;
}

ULONG GetDACOffset_tr_plus(int CardNb, unsigned long int *myDACOffset_tr_plus){
    *myDACOffset_tr_plus = AllCards[CardNb].DACOffset_tr_plus;
    return 0;
}

ULONG GetDACOffset_tr_minus(int CardNb, unsigned long int *myDACOffset_tr_minus){

    *myDACOffset_tr_minus = AllCards[CardNb].DACOffset_tr_minus;

    return 0;
}

/// * SETTERS

ULONG SetVGA(int CardNb, int VGA_nb, DWORD myVGA){

    if (VGA_nb ==  1)

	AllCards[CardNb].VGA1 = myVGA;

    else

	AllCards[CardNb].VGA2 = myVGA;

    UpdateVGA(CardNb, VGA_nb);

    return 0;
}


ULONG SetTriggerType(int CardNb, DWORD myTriggerType){

    AllCards[CardNb].TriggerType = myTriggerType;
    // ftStatus 	   		= Update(CardNb, myTriggerType);
    // checkout

    return 0;
}

ULONG SetDigitalThreshold(int CardNb, DWORD myDigitalThreshold){

    AllCards[CardNb].DigitalThreshold = myDigitalThreshold;
    // ftStatus 	   = Update(CardNb, myDigitalThreshold);
    // checkout

    return 0;
}

ULONG SetFrequency(int CardNb, unsigned short int myFrequency){

    AllCards[CardNb].Frequency 	= myFrequency;
    ftStatus 	   	  			= UpdateFrequency(CardNb);
    checkout

	ftStatus = ResetPLL(CardNb);
    checkout

	ftStatus = ResetDDRClock(CardNb);
    checkout

	return 0;
}

ULONG SetFrequencyParams(int CardNb, const char MyParams[16]){

    // AllCards[CardNb].Frequency 	= myFrequency;
    ftStatus = UpdateFrequencyParams(CardNb, MyParams);
    checkout

	ftStatus = ResetPLL(CardNb);
    checkout

	ftStatus = ResetDDRClock(CardNb);
    checkout

	return 0;
}

ULONG SetEscale(int CardNb, DWORD myEscale){

    AllCards[CardNb].Escale = myEscale;
    ftStatus 	   			= UpdateEscale(CardNb);
    checkout

	return 0;
}

ULONG SetADCOffset(int CardNb, DWORD myADCOffset){

    AllCards[CardNb].ADCOffset = myADCOffset;
    ftStatus = UpdateADCOffset(CardNb);
    checkout

	return 0;

}

/// ** DAC
// A, 2 , Trigger Ext -
// B, 3,  Trigger Ext +
// C, 4,  Test -
// D, 5,  Test +
// E, 12, offset +
// F, 13, Offset -
// G, 14, Trigger -
// H, 15, Trigger +

ULONG SetDACOffset_tr_ext_minus(int CardNb, unsigned long int myDACOffset_tr_ext_minus){
    AllCards[CardNb].DACOffset_tr_ext_minus = myDACOffset_tr_ext_minus;
    ftStatus = UpdateDACOffset(CardNb, 3, 0, myDACOffset_tr_ext_minus);
    checkout
	return 0;
}

ULONG SetDACOffset_tr_ext_plus(int CardNb, unsigned long int myDACOffset_tr_ext_plus){
    AllCards[CardNb].DACOffset_tr_ext_plus = myDACOffset_tr_ext_plus;
    ftStatus = UpdateDACOffset(CardNb, 3, 1, myDACOffset_tr_ext_plus);
    checkout
	return 0;
}

ULONG SetDACOffset_test_minus(int CardNb, unsigned long int myDACOffset_test_minus){
    AllCards[CardNb].DACOffset_test_minus = myDACOffset_test_minus;
    ftStatus = UpdateDACOffset(CardNb, 3, 2, myDACOffset_test_minus);
    checkout
	return 0;
}

ULONG SetDACOffset_test_plus(int CardNb, unsigned long int myDACOffset_test_plus){
    AllCards[CardNb].DACOffset_test_plus = myDACOffset_test_plus;
    ftStatus = UpdateDACOffset(CardNb, 3, 3, myDACOffset_test_plus);
    checkout
	return 0;
}

ULONG SetDACOffset_offset_plus(int CardNb, unsigned long int myDACOffset_offset_plus){
    AllCards[CardNb].DACOffset_offset_plus = myDACOffset_offset_plus;
    ftStatus = UpdateDACOffset(CardNb, 3, 4, myDACOffset_offset_plus);
    checkout
	return 0;
}

ULONG SetDACOffset_offset_minus(int CardNb, unsigned long int myDACOffset_offset_minus){
    AllCards[CardNb].DACOffset_offset_minus = myDACOffset_offset_minus;
    ftStatus = UpdateDACOffset(CardNb, 3, 5, myDACOffset_offset_minus);
    checkout
	return 0;
}

ULONG SetDACOffset_tr_minus(int CardNb, unsigned long int myDACOffset_tr_minus){
    AllCards[CardNb].DACOffset_tr_minus = myDACOffset_tr_minus;
    ftStatus = UpdateDACOffset(CardNb, 3, 6, myDACOffset_tr_minus);
    checkout
	return 0;
}

ULONG SetDACOffset_tr_plus(int CardNb, unsigned long int myDACOffset_tr_plus){
    AllCards[CardNb].DACOffset_tr_plus = myDACOffset_tr_plus;
    ftStatus = UpdateDACOffset(CardNb, 3, 7, myDACOffset_tr_plus);
    checkout
	return 0;
}

/// * Configure Board

ULONG ConfigureBoard(int CardNb){

    ftStatus = PurgeRxTxUSB(CardNb);
    checkout

	// Set the default Frequency of all open devices
	ftStatus = UpdateFrequency(CardNb);
    checkout

	ftStatus = ResetPLL(CardNb);
    checkout

	//ftStatus = ResetFPGACLocks(ftHandle);
	//checkout
	//Sleep(500);

	ftStatus = ResetDDRClock(CardNb);
    checkout

	ftStatus = ResetMIG_IP(CardNb);
    checkout

	// Set the default init values
	if (AllCards[CardNb].IsOpen ==  1){
	    // Set the default Escale of all open devices
	    ftStatus = UpdateEscale(CardNb);
	    checkout
		// Set the default ADCOffset of all open devices
		ftStatus = UpdateADCOffset(CardNb);
	    checkout
		//
		ftStatus = UpdateDACOffset(CardNb, 3, 0, AllCards[CardNb].DACOffset_tr_ext_minus);
	    ftStatus = UpdateDACOffset(CardNb, 3, 1, AllCards[CardNb].DACOffset_tr_ext_plus);
	    ftStatus = UpdateDACOffset(CardNb, 3, 2, AllCards[CardNb].DACOffset_test_minus);
	    ftStatus = UpdateDACOffset(CardNb, 3, 3, AllCards[CardNb].DACOffset_test_plus);
	    ftStatus = UpdateDACOffset(CardNb, 3, 4, AllCards[CardNb].DACOffset_offset_plus);
	    ftStatus = UpdateDACOffset(CardNb, 3, 5, AllCards[CardNb].DACOffset_offset_minus);
	    ftStatus = UpdateDACOffset(CardNb, 3, 6, AllCards[CardNb].DACOffset_tr_minus);
	    ftStatus = UpdateDACOffset(CardNb, 3, 7, AllCards[CardNb].DACOffset_tr_plus);
	}

    /* A, 2 , Trigger Ext - */
    /* B, 3,  Trigger Ext + */
    /* C, 4,  Test - */
    /* D, 5,  Test + */
    /* E, 12, offset + */
    /* F, 13, Offset - */
    /* G, 14, Trigger - */
    /* H, 15, Trigger + */

    // set adc offset
    //
    // COMMAND table
    //
    // C3 C2 C1 C0
    // 0 0 0 0 Write to Input Register n
    // 0 0 0 1 Update (Power Up) DAC Register n
    // 0 0 1 0 Write to Input Register n, Update (Power Up) All n
    // 0 0 1 1 Write to and Update (Power Up) n
    // 0 1 0 0 Power  Down  n
    // 1 1 1 1 No  Operation
    //
    // ADDRESS (n)*
    //
    // A3 A2 A1 A0
    // 0 0 0 0 DAC A
    // 0 0 0 1 DAC B
    // 0 0 1 0 DAC C
    // 0 0 1 1 DAC D
    // 0 1 0 0 DAC E
    // 0 1 0 1 DAC F
    // 0 1 1 0 DAC G
    // 0 1 1 1 DAC H
    // 1 1 1 1 All DAC

    ftStatus = UpdateControlRegister(CardNb);
    checkout

	return 0;
}

ULONG ResetPLL(int CardNb){

    // send reset to the pll
    char lpBuffer[2];
    lpBuffer[0] = 0x02;
    lpBuffer[1] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 2, &BytesWritten);
    checkout

	ftStatus = NullCommand(CardNb);
    checkout

	return 0;
}

ULONG UpdateFrequency(int CardNb){

    char lpBuffer[22];
    lpBuffer[0] = 0x03; // command

    switch (AllCards[CardNb].Frequency){
    case 500 :
	lpBuffer[1] = 0x00;	lpBuffer[2] = 0x80;	lpBuffer[3] = 0x01;	lpBuffer[4] = 0x00;
	break;
    default :
	lpBuffer[1] = 0x00;	lpBuffer[2] = 0x70;	lpBuffer[3] = 0x00;	lpBuffer[4] = 0x00;
	break;
    };

    lpBuffer[5] = 0x7D;		lpBuffer[6] = 0x00;		lpBuffer[7] = 0x07;		lpBuffer[8] = 0x00;
    lpBuffer[9] = 0xF2; 	lpBuffer[10]= 0xC4;		lpBuffer[11]= 0x76; 	lpBuffer[12]= 0xD0;
    lpBuffer[13] = 0x0B; 	lpBuffer[14] = 0x00; 	lpBuffer[15] = 0x00; 	lpBuffer[16] = 0x00;
    lpBuffer[17] = 0xF2; 	lpBuffer[18] = 0xC4; 	lpBuffer[19] = 0x76; 	lpBuffer[20] = 0xD0;

    lpBuffer[21] = 0xFF; // packet end

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 22, &BytesWritten);
    checkout

	return 0;
}

ULONG UpdateFrequencyParams(int CardNb, const char MyParams[16]){

    char lpBuffer[22];

    lpBuffer[0] = 0x03;
    int i;

    for (i=0; i<16; i++){
	lpBuffer[i+1] = MyParams[i];
    }
    for (i=0; i<4; i++){
	lpBuffer[i+17] = MyParams[8+i];
    }
    lpBuffer[21] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 22, &BytesWritten);
    checkout

	return 0;
}

ULONG ResetDDRClock(int CardNb){

    // sends reset to the pll
    char lpBuffer[2];
    lpBuffer[0] = 0x0E;
    lpBuffer[1] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 2, &BytesWritten);
    checkout

	// release - zero command
	ftStatus = NullCommand(CardNb);
    checkout

	return 0;
}

ULONG ResetMIG_IP(int CardNb){

    // send reset to the IP
    char lpBuffer[2];
    lpBuffer[0] = 0x07;
    lpBuffer[1] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 2, &BytesWritten);
    checkout

	// release - zero command
	ftStatus = NullCommand(CardNb);
    checkout

	return 0;
}

ULONG UpdateADCOffset(int CardNb){

    char lpBuffer[5];
    lpBuffer[0] = 0x0C;
    lpBuffer[1] = (char)AllCards[CardNb].ADCOffset;
    lpBuffer[2] = 0x00;
    lpBuffer[3] = 0x00;
    lpBuffer[4] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 5, &BytesWritten);
    checkout

	return 0;
}

ULONG UpdateVGA(int CardNb, int VGA_nb){

    char lpBuffer[4];

    lpBuffer[0] = 0x09;
    if (VGA_nb ==  1){
	lpBuffer[1] = (char)AllCards[CardNb].VGA1;
	lpBuffer[2] = 0x01;
    }
    else{
	lpBuffer[1] = (char)AllCards[CardNb].VGA2;
	lpBuffer[2] = 0x00;
    }
    lpBuffer[3] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 4, &BytesWritten);
    checkout

	return 0;
}

ULONG UpdateDACOffset(int CardNb, unsigned short int dac_command,
		      unsigned short int dac_address, unsigned short int dac_value){

    char lpBuffer[6];
    lpBuffer[0] = 0x0D;
    lpBuffer[1] = dac_command & 0xFF;
    lpBuffer[2] = dac_address & 0xFF;
    lpBuffer[3] = dac_value & 0xFF;
    lpBuffer[4] = (dac_value & 0xFF00) >> 8;
    lpBuffer[5] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 6, &BytesWritten);
    checkout

	return 0;
}

ULONG UpdateControlRegister(int CardNb){

    char lpBuffer[2]; // command
    lpBuffer[0] = 0x0A;
    lpBuffer[1] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 2, &BytesWritten);
    checkout

	ftStatus = ReadUSB(CardNb, ControlRegister, 1, &BytesRead);
    checkout

	return 0;
}

ULONG SetOscillogramMode(int CardNb, unsigned short int Length){

    ftStatus = PurgeRxTxUSB(CardNb);
    checkout

	char lpBuffer[6];
    lpBuffer[0] = 0x06;
    lpBuffer[1] = AllCards[CardNb].TriggerType & 0x001F;
    lpBuffer[2] = Length & 0x00FF;
    lpBuffer[3] = AllCards[CardNb].DigitalThreshold & 0xFF;
    lpBuffer[4] = (AllCards[CardNb].DigitalThreshold & 0x0F00) >> 8;
    lpBuffer[5] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 6, &BytesWritten);
    checkout

	return 0;
}

ULONG SetRampMode(int CardNb){

    ftStatus = PurgeRxTxUSB(CardNb);
    checkout

	char lpBuffer[4];
    lpBuffer[0] = 0x01;
    lpBuffer[1] = 0;
    lpBuffer[2] = 0x000A & 0x00FF;
    lpBuffer[3] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 4, &BytesWritten);
    checkout

	return 0;
}

ULONG SetUSBTestMode(int CardNb){

    ftStatus = PurgeRxTxUSB(CardNb);
    checkout

	char lpBuffer[2];
    lpBuffer[0] = 0x04;
    lpBuffer[1] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 2, &BytesWritten);
    checkout

	return 0;
}

ULONG UpdateEscale(int CardNb){

    char lpBuffer[3];
    lpBuffer[0] = 0x0B;
    lpBuffer[1] = 0x00;
    lpBuffer[2] = 0xFF;

    switch (AllCards[CardNb].Escale){
    case 1:  lpBuffer[1] = 0x01; break;
    case 0:  lpBuffer[1] = 0x00; break;
    default: lpBuffer[1] = 0x00; break;
    }

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 3, &BytesWritten);
    checkout

	return 0;
}

/// * HELPER

ULONG NullCommand(int CardNb){

    // null command
    char lpBuffer[2];
    lpBuffer[0] = 0x00;
    lpBuffer[1] = 0xFF;

    ftStatus = WriteUSB(CardNb, (void *)lpBuffer, 2, &BytesWritten);
    checkout

	return 0;
}

ULONG PurgeRxTxUSB(int CardNb){

    int i = 0;

    for (i=0;i<2;i++){

	// do { ftStatus = FT_StopInTask(ftHandle[CardNb]); } while (ftStatus != FT_OK);

	ftStatus = NullCommand(CardNb);
	checkout
	    sleep(.2);

	// purge
	ftStatus = FT_Purge(ftHandle[CardNb], FT_PURGE_RX | FT_PURGE_TX);
	checkout
	    //Sleep(200);

	    // do { ftStatus = FT_RestartInTask(ftHandle); } while (ftStatus != FT_OK);

	    }

    return 0;
}

ULONG PurgeRxTxUSB_test(){

    //int i = 0;

    // for (i=0;i<2;i++){

    // ftStatus = NullCommand(0);
    // checkout
    // Sleep(200);

    do { ftStatus = FT_StopInTask(ftHandle[0]); } while (ftStatus != FT_OK);

    // purge
    // ftStatus = FT_Purge(ftHandle[0], FT_PURGE_RX | FT_PURGE_TX);
    // checkout
    // Sleep(200);

    do { ftStatus = FT_RestartInTask(ftHandle); } while (ftStatus != FT_OK);

    //}

    return ftStatus;
}

ULONG WriteUSB(int CardNb, void * Buffer, DWORD BytesToWrite, LPDWORD BytesWritten){

    ftStatus = FT_Write(ftHandle[CardNb], Buffer, BytesToWrite, BytesWritten);
    checkout
	sleep(.5);

    if (*BytesWritten != BytesToWrite)
	return 20;

    return 0;
}

ULONG ReadUSB(int CardNb, LPVOID Buffer, DWORD BytesToRead, LPDWORD BytesRead){

    ftStatus = FT_Read(ftHandle[CardNb], Buffer, BytesToRead, BytesRead);
    checkout
	if (*BytesRead != BytesToRead)
	    return 50;

    return 0;
}
