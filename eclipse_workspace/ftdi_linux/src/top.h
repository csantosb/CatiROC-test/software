#ifndef TOP_H_
#define TOP_H_

#define checkout if (ftStatus != FT_OK) return ftStatus;

// * USB CONSTANTS
#define InTransferSize	65536
#define OutTransferSize 65536
#define ReadTimeout	2000
#define WriteTimeout	2000
#define Mask		0xff
#define Mode		0x40
#define uXon		0
#define uXoff		0
#define NumMaxOfDevices 5

typedef struct
{
DWORD Escale;
DWORD IsOpen;
DWORD ADCOffset;
DWORD DigitalThreshold;
DWORD TriggerType;
unsigned short int Frequency;
unsigned long int DACOffset_tr_ext_plus;
unsigned long int DACOffset_tr_ext_minus;
unsigned long int DACOffset_test_plus;
unsigned long int DACOffset_test_minus;
unsigned long int DACOffset_offset_plus;
unsigned long int DACOffset_offset_minus;
unsigned long int DACOffset_tr_plus;
unsigned long int DACOffset_tr_minus;
unsigned long int VGA1;
unsigned long int VGA2;
} Card;


// * PROTOTYPES

// ** usb.h ______________________________________
// Getters
ULONG GetRxTxStatusUSB(int CardNb, LPDWORD myAmountInRxQueue, LPDWORD myAmountInTxQueue, LPDWORD myEventStatus);
ULONG GetNumberDevs(LPDWORD myNumberDevs);
ULONG GetDescriptors(char SerialNumberOut[16], char DescriptionOut[64]);
// Init / End
ULONG InitializeUSB(int CardNb);
ULONG FinalizeUSB(int CardNb);
// Conf
ULONG ConfigureUSB(int CardNb);
// status
ULONG UpdateRxTxStatusUSB(int CardNb);
ULONG UpdateRxStatusUSB(int CardNb);


/// ** board.h ______________________________________
// Getters
ULONG GetTriggerType(int CardNb, DWORD *myTriggerType);
ULONG GetDigitalThreshold(int CardNb, DWORD *myDigitalThreshold);
ULONG GetControlRegister(int CardNb, char ControlRegisterOut[1]);
ULONG GetIsOpen(int CardNb, DWORD *myIsOpen);
ULONG GetEscale(int CardNb, DWORD *myEscale);
ULONG GetFrequency(int CardNb, unsigned short int *myFrequency);
ULONG GetADCOffset(int CardNb, DWORD *myADCOffset);
ULONG GetVGA(int CardNb, int VGA_nb, DWORD *myVGA);
// DAC
ULONG GetDACOffset_tr_ext_plus(int CardNb, unsigned long int *myDACOffset_tr_ext_plus);
ULONG GetDACOffset_tr_ext_minus(int CardNb, unsigned long int *myDACOffset_tr_ext_minus);
ULONG GetDACOffset_test_plus(int CardNb, unsigned long int *myDACOffset_test_plus);
ULONG GetDACOffset_test_minus(int CardNb, unsigned long int *myDACOffset_test_minus);
ULONG GetDACOffset_offset_plus(int CardNb, unsigned long int *myDACOffset_offset_plus);
ULONG GetDACOffset_offset_minus(int CardNb, unsigned long int *myDACOffset_offset_minus);
ULONG GetDACOffset_tr_plus(int CardNb, unsigned long int *myDACOffset_tr_plus);
ULONG GetDACOffset_tr_minus(int CardNb, unsigned long int *myDACOffset_tr_minus);
// Setters
ULONG SetTriggerType(int CardNb, DWORD myTriggerType);
ULONG SetDigitalThreshold(int CardNb, DWORD myDigitalThreshold);
ULONG SetFrequency(int CardNb, unsigned short int myFrequency);
ULONG SetFrequencyParams(int CardNb, const char MyParams[16]);
ULONG SetEscale(int CardNb, DWORD myEscale);
ULONG SetADCOffset(int CardNb, DWORD offset);
ULONG SetVGA(int CardNb, int VGA_nb, DWORD myVGA);
// DAC
ULONG SetDACOffset_tr_ext_plus(int CardNb, unsigned long int myDACOffset_tr_ext_plus);
ULONG SetDACOffset_tr_ext_minus(int CardNb, unsigned long int myDACOffset_tr_ext_minus);
ULONG SetDACOffset_test_plus(int CardNb, unsigned long int myDACOffset_test_plus);
ULONG SetDACOffset_test_minus(int CardNb, unsigned long int myDACOffset_test_minus);
ULONG SetDACOffset_offset_plus(int CardNb, unsigned long int myDACOffset_offset_plus);
ULONG SetDACOffset_offset_minus(int CardNb, unsigned long int myDACOffset_offset_minus);
ULONG SetDACOffset_tr_plus(int CardNb, unsigned long int myDACOffset_tr_plus);
ULONG SetDACOffset_tr_minus(int CardNb, unsigned long int myDACOffset_tr_minus);
// Conf
ULONG ConfigureBoard(int CardNb);
// Reset
ULONG ResetPLL(int CardNb);
ULONG ResetDDRClock(int CardNb);
ULONG ResetMIG_IP(int CardNb);
// Update Params
ULONG UpdateFrequency(int CardNb);
ULONG UpdateFrequencyParams(int CardNb, const char MyParams[16]);
ULONG UpdateDACOffset(int CardNb, unsigned short int dac_command, unsigned short int dac_address, unsigned short int dac_value);
ULONG SetRampMode(int CardNb);
ULONG SetUSBTestMode(int CardNb);
ULONG SetOscillogramMode(int CardNb, unsigned short int Length);
ULONG UpdateEscale(int CardNb);
ULONG UpdateADCOffset(int CardNb);
ULONG UpdateVGA(int CardNb, int VGA_nb);
// Get Params
ULONG UpdateControlRegister(int CardNb);
//helper
ULONG PurgeRxTxUSB(int CardNb);
ULONG NullCommand(int CardNb);
ULONG WriteUSB(int CardNb, void * Buffer, DWORD BytesToWrite, LPDWORD BytesWritten);
ULONG ReadUSB(int CardNb, LPVOID Buffer, DWORD BytesToRead, LPDWORD BytesRead);

// * GLOBALS

// usb.h ______________________________________
FT_STATUS ftStatus;
UCHAR BitMode;
UCHAR LatencyTimer;
DWORD BytesWritten;
DWORD BytesRead;

FT_HANDLE ftHandle[NumMaxOfDevices];
DWORD NumberDevs;
DWORD Flags[NumMaxOfDevices];
DWORD Type[NumMaxOfDevices];
DWORD ID[NumMaxOfDevices];
DWORD LocId[NumMaxOfDevices];

#endif /* TOP_H_ */
