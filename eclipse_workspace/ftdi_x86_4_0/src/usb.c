#include <windows.h>
#include "ftd2xx.h"
#include "top.h"

extern DWORD IsOpen[NumMaxOfDevices];
extern Card AllCards[NumMaxOfDevices];
DWORD AmountInRxQueue[NumMaxOfDevices];
DWORD AmountInTxQueue[NumMaxOfDevices];
DWORD EventStatus[NumMaxOfDevices];
char SerialNumber[16];
char Description[64];

/////////////
// GETTERS //
/////////////

ULONG GetRxTxStatusUSB(int CardNb, LPDWORD myAmountInRxQueue, LPDWORD myAmountInTxQueue, LPDWORD myEventStatus){

    ftStatus = UpdateRxTxStatusUSB(CardNb);
    checkout

	*myAmountInRxQueue	= AmountInRxQueue[CardNb];
    *myAmountInTxQueue	= AmountInRxQueue[CardNb];
    *myEventStatus		= EventStatus[CardNb];

    return 0;
}

ULONG GetNumberDevs(LPDWORD myNumberDevs){

    ftStatus = FT_CreateDeviceInfoList(&NumberDevs);
    checkout

	*myNumberDevs = NumberDevs>>1;

    return 0;
}

ULONG GetDescriptors(char mySerialNumber[16], char myDescription[64]){

    strcpy(mySerialNumber, SerialNumber);
    strcpy(myDescription, Description);

    return 0;

}

////////////////
// INIT / END //
////////////////

ULONG InitializeUSB(int CardNb){

    // create the device information list
    ftStatus = FT_CreateDeviceInfoList(&NumberDevs);
    checkout
	Sleep(100);

    ftStatus = FT_GetDeviceInfoDetail(CardNb, Flags + CardNb, Type + CardNb, ID + CardNb, LocId + CardNb,
				      SerialNumber, Description, ftHandle + CardNb);
    checkout
	Sleep(100);

    ftStatus = FT_Open(CardNb, ftHandle + CardNb);
    checkout
	Sleep(100);

    AllCards[CardNb].IsOpen = 1;

    return 0;
}

ULONG FinalizeUSB(int CardNb){

    FT_Close(ftHandle[CardNb]);
    checkout

	AllCards[CardNb].IsOpen = 0;

    return 0;
}

//////////////////
// Configure USB//
//////////////////

ULONG ConfigureUSB(int CardNb){

    ftStatus = FT_ResetDevice(ftHandle[CardNb]);
    checkout

	ftStatus = FT_SetTimeouts(ftHandle[CardNb], ReadTimeout, WriteTimeout);
    checkout

	// Get Bit Mode
	ftStatus = FT_GetBitMode(ftHandle[CardNb], &BitMode);
    checkout
	Sleep(100);

    // Reset Bit Mode
    ftStatus = FT_SetBitMode(ftHandle[CardNb], Mask, 0x00);
    checkout
	Sleep(100);

    // Set Bit Mode
    ftStatus = FT_SetBitMode(ftHandle[CardNb], Mask, Mode);
    checkout
	Sleep(100);

    // Get Latency Timer
    ftStatus = FT_GetLatencyTimer(ftHandle[CardNb], &LatencyTimer);
    checkout

	// Set Latency Timer
	UCHAR LatencyTimer2 = 2;
    ftStatus = FT_SetLatencyTimer(ftHandle[CardNb], LatencyTimer2);
    checkout

	// Set USB Parameters
	ftStatus = FT_SetUSBParameters(ftHandle[CardNb], InTransferSize, OutTransferSize);
    checkout

	// Flow Control
	ftStatus = FT_SetFlowControl(ftHandle[CardNb], FT_FLOW_RTS_CTS, uXon, uXoff);
    checkout

	return 0;
}

/////////////////
// USB STATUS  //
/////////////////

ULONG UpdateRxTxStatusUSB(int CardNb){

    ftStatus = FT_GetStatus(ftHandle[CardNb], AmountInRxQueue + CardNb, AmountInTxQueue + CardNb, EventStatus + CardNb);
    checkout

	return 0;
}

ULONG UpdateRxStatusUSB(int CardNb){

    ftStatus = FT_GetQueueStatus(ftHandle[CardNb], AmountInRxQueue + CardNb);
    checkout

	return 0;
}
